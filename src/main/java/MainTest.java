import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;

import com.stemstudios.cassandra.directory.CassandraDirectory;
import com.stemstudios.cassandra.directory.CassandraDirectoryConfig;

public class MainTest
{

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args)
	{
		try
		{
			BasicConfigurator.configure();
			String CasAddress = "192.168.0.101";
			int CasPort = 9160;
			String CasCluster = "TestCluster";
			String CasKeySpace = "DEMO";
			String CasCF = "TestIndex";
			
			CassandraDirectoryConfig config = new CassandraDirectoryConfig(CasAddress, CasPort, CasCluster, CasKeySpace, CasCF);

			CassandraDirectory dir = new CassandraDirectory(config);
			Analyzer standardAnalyzer = new StandardAnalyzer(Version.LUCENE_40);

			IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_40, standardAnalyzer);
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			IndexWriter writer = new IndexWriter(dir, iwc);
			for (int i = 0; i < 10000;i++)
			{
				Document doc = new Document();
				FieldType defType = new FieldType();
				defType.setIndexed(true);
				doc.add(new Field("id", i+"", defType));
				doc.add(new Field("title", "The big book of boredom - Part " + i, defType));
				writer.addDocument(doc);
			}
			System.out.println("Num stored documents "+writer.numDocs()+" number of docs in memory "+writer.numRamDocs());
			writer.maybeMerge();
			writer.close();

			IndexSearcher srch = new IndexSearcher(DirectoryReader.open(dir));
			QueryParser parser = new QueryParser(Version.LUCENE_40, "title", standardAnalyzer);
			Date start = new Date();
			Query qry = parser.parse("title:\"1\"");
			Set<Term> terms = new HashSet<Term>();
			qry.extractTerms(terms);
			System.out.println("Hits: " + srch.search(qry, null, 10).scoreDocs.length);

			Date end = new Date();
			System.out.println("Time: " + (end.getTime() - start.getTime()) + "ms");
			dir.close();
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

}
