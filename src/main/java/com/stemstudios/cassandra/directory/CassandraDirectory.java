package com.stemstudios.cassandra.directory;

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.IOContext;
import org.apache.lucene.store.IndexInput;
import org.apache.lucene.store.IndexOutput;
import org.apache.lucene.store.Lock;
import org.apache.lucene.store.NoSuchDirectoryException;

/**
 * The <code>CassandraDirectory</code> maps the concept of a Lucene directory to
 * a column family that belongs to a certain keyspace located in a given
 * Cassandra server. Furthermore, it stores each file under this directory as a
 * row in that column family.
 * 
 * <p>
 * In particular, files are broken down into blocks (whose sizes are capped),
 * where each block (see {@link FileBlock}) is stored as the value of a column
 * in the corresponding row. As per
 * http://wiki.apache.org/cassandra/CassandraLimitations, this is the
 * recommended approach for dealing with large objects, which Lucene files tend
 * to be. Furthermore, a descriptor of the file (see {@link FileDescriptor})
 * that outlines a map of blocks therein is stored as one of the columns in that
 * row as well. Think of this descriptor as an inode for Cassandra-based files.
 * </p>
 * 
 * <p>
 * The exhaustive mapping of a Lucene directory (file) to a Cassandra column
 * family (row) is captured in the {@link ColumnOrientedDirectory} (
 * {@link ColumnOrientedFile}) inner-class. Specifically, they interpret
 * Cassandra's data model in terms of that of Lucene. More importantly, these
 * are the only two inner-classes that have a foot in both the Lucene and
 * Cassandra camps.
 * </p>
 * 
 * <p>
 * All writes to a file in this directory occur through a
 * {@link CassandraIndexOutput}, which puts the data flushed from a write-behind
 * buffer into the fitting set of blocks. By the same token, all reads from a
 * file in this directory occur through a {@link CassandraIndexInput}, which
 * gets the data needed by a read-ahead buffer from the right set of blocks.
 * </p>
 * 
 * <p>
 * The last (but not the least) inner-class, {@link CassandraClient}, acts as a
 * facade for a Thrift-based Cassandra client. In short, it provides operations
 * to get/put rows/columns in the column family and keyspace associated with
 * this directory.
 * </p>
 * 
 * <p>
 * Unlike Lucandra, which attempts to bridge the gap between Lucene and
 * Cassandra at the document-level, the {@link CassandraDirectory} is
 * self-sufficient in the sense that it does not require a re-write of other
 * components in the Lucene stack. In other words, one may use the
 * {@link CassandraDirectory} in conjunction with the {@link IndexWriter} and
 * {@link IndexReader}, as you would any other kind of Lucene {@link Directory}.
 * Moreover, given the the data unit that is transferred to and from Cassandra
 * is a large-sized block, one may expect fewer round trips, and hence better
 * throughputs, from the {@link CassandraDirectory}.
 * <p>
 * 
 * <p>
 * In conclusion, this directory attempts to marry the rich search-based query
 * language of Lucene with the distributed fault-tolerant database that is
 * Cassandra. By delegating the responsibilities of replication, durability and
 * elasticity to the directory, we free the layers above from such
 * non-functional concerns. Our hope is that users will choose to make their
 * large-scale indices instantly scalable by seamlessly migrating them to this
 * type of directory (using {@link Directory#copyTo(Directory)}).
 * </p>
 * 
 * @author Karthick Sankarachary
 */
public class CassandraDirectory extends Directory
{
	private Log log = LogFactory.getLog(CassandraDirectory.class);

	// The name of every column that holds a file block starts with this prefix.
	protected static final String BLOCK_COLUMN_NAME_PREFIX = "BLOCK-";

	// The name of the column that holds the file descriptor.
	protected static final String descriptorColumn = "DESCRIPTOR";

	// The list of meta-columns currently defined for each file (or row).
	protected static final List<ByteBuffer> systemColumns = new ArrayList<ByteBuffer>();
	static
	{
		systemColumns.add(ByteBuffer.wrap(descriptorColumn.getBytes()));
	}

	// The reference to the cassandra client that talks to the thrift server.
	protected CassandraClient cassandraClient;

	// The keyspace in which to read/write cassandra directories and files.
	protected String keyspace;

	// The name of the column family that maps to this cassandra directory.
	protected String columnFamily;

	// The current size of the block to write out to a column.
	protected int blockSize;

	// The current size of the buffer that (ideally) is big enough to hold one
	// or
	// more file blocks. In essence, the write (read) buffer acts as a
	// write-behind (read-ahead) cache that performs a lazy write (read) only
	// when
	// the data is evicted (or flushed) from the cache. Given that, we should
	// try
	// to write and read data in block multiples.
	protected int bufferSize;

	protected ColumnOrientedDirectory columnOrientedDirectory;
	protected final CassandraDirectoryConfig config;

	/**
	 * Creates a Cassandra Directory object using the configuration provided by
	 * the CassandraDirectoryConfig object.
	 * 
	 * @param config
	 *            - Configuration object used to set up the directory
	 */
	public CassandraDirectory(CassandraDirectoryConfig config)
	{
		this.config = config;
		this.config.Start();
		this.blockSize = config.getBlockSize();
		this.bufferSize = blockSize;

		this.cassandraClient = new CassandraClient(config);
		this.columnOrientedDirectory = new ColumnOrientedDirectory(this);
		log.info("Opened Cassandra Directory for Index " + config.getColumnFamily().getName());
	}

	/**
	 * Closes all the client-side resources obtained by this directory instance.
	 */
	@Override
	public void close() throws IOException
	{
		config.Shutdown();
		isOpen = false;
	}

	/**
	 * Creates a new, empty file in the directory with the given file name.
	 * 
	 * @return a stream that writes into this file
	 */
	public IndexOutput createOutput(String fileName) throws IOException
	{
		ensureOpen();
		FileDescriptor fileDesc = this.columnOrientedDirectory.getFileDescriptor(fileName, true);
		return new CassandraIndexOutput(this, fileDesc);
	}

	@Override
	public IndexOutput createOutput(String fileName, IOContext arg1) throws IOException
	{
		ensureOpen();
		FileDescriptor fileDesc = this.columnOrientedDirectory.getFileDescriptor(fileName, true);
		return new CassandraIndexOutput(this, fileDesc);
	}

	/**
	 * Removes an existing file in the directory.
	 */
	@Override
	public void deleteFile(String fileName) throws IOException
	{
		ensureOpen();
		FileDescriptor fileDescriptor = columnOrientedDirectory.getFileDescriptor(fileName);
		if (fileDescriptor != null)
		{
			fileDescriptor.setDeleted(true);
			columnOrientedDirectory.setFileDescriptor(fileDescriptor);
		}
	}

	/**
	 * @return true iff a file with the given name exists
	 */
	@Override
	public boolean fileExists(String fileName) throws IOException
	{
		ensureOpen();
		try
		{
			return fileLength(fileName) >= 0;
		} catch (IOException e)
		{
			return false;
		}
	}

	/**
	 * Returns the length of a file in the directory. This method follows the
	 * following contract:
	 * <ul>
	 * <li>Throws {@link FileNotFoundException} if the file does not exist
	 * <li>Returns a value &ge;0 if the file exists, which specifies its length.
	 * </ul>
	 * 
	 * @param name
	 *            the name of the file for which to return the length.
	 * @throws FileNotFoundException
	 *             if the file does not exist.
	 * @throws IOException
	 *             if there was an IO error while retrieving the file's length.
	 */
	@Override
	public long fileLength(String fileName) throws IOException
	{
		ensureOpen();
		FileDescriptor descriptor = columnOrientedDirectory.getFileDescriptor(fileName);
		if (descriptor == null)
		{
			throw new IOException("Could not find descriptor for file " + fileName);
		}
		return descriptor.getLength();
	}

	/**
	 * @return the time the named file was last modified
	 */
	public long fileModified(String fileName) throws IOException
	{
		ensureOpen();
		FileDescriptor descriptor = columnOrientedDirectory.getFileDescriptor(fileName);
		if (descriptor == null)
		{
			throw new IOException("Could not find descriptor for file " + fileName);
		}
		return descriptor.getLastModified();
	}

	/**
	 * @return the size of the file block
	 */
	public long getBlockSize()
	{
		return blockSize;
	}

	/**
	 * @return the size of the read/write buffer
	 */
	public long getBufferSize()
	{
		return bufferSize;
	}

	/**
	 * @return the name of the column family that represents this directory
	 */
	public String getColumnFamily()
	{
		return columnFamily;
	}

	/**
	 * @return the name of the keyspace in which to find the column family
	 */
	public String getKeyspace()
	{
		return keyspace;
	}

	/**
	 * Returns an array of strings, one for each (non-deleted) file in the
	 * directory.
	 * 
	 * @throws NoSuchDirectoryException
	 *             if the directory is not prepared for any write operations
	 *             (such as {@link #createOutput(String)}).
	 * @throws IOException
	 *             in case of other IO errors
	 */
	@Override
	public String[] listAll() throws IOException
	{
		ensureOpen();
		String[] names = columnOrientedDirectory.getFileNames();
		if (names != null)
		{
			return names;
		} else
		{
			throw new IOException("Column Family '" + config.getColumnFamily().getName() + "' exists, but cannot be listed: list() returned null");
		}
	}

	public Lock makeLock(String name)
	{
		CassLockFactory factory = new CassLockFactory();
		return factory.makeLock(config, name);
	}

	/**
	 * Open an existing file in the directory with the given file name.
	 * 
	 * @return a stream that reads from an existing file.
	 */
	public IndexInput openInput(String fileName) throws IOException
	{
		ensureOpen();
		FileDescriptor fileDes = columnOrientedDirectory.getFileDescriptor(fileName, true);
		return new CassandraIndexInput(this, fileDes);
	}

	@Override
	public IndexInput openInput(String fileName, IOContext arg1) throws IOException
	{
		return openInput(fileName);
	}

	@Override
	public void sync(Collection<String> arg0) throws IOException
	{
		// TODO Auto-generated method stub
		System.out.println("Sync Called");
	}

	/**
	 * Set the modified time of an existing file to now.
	 */
	public void touchFile(String fileName) throws IOException
	{
		ensureOpen();
		try
		{
			FileDescriptor fileDescriptor = columnOrientedDirectory.getFileDescriptor(fileName);
			fileDescriptor.setLastModified(System.currentTimeMillis());
			columnOrientedDirectory.setFileDescriptor(fileDescriptor);
		} catch (Exception e)
		{
			throw new IOException("Could not touch file " + fileName, e);
		}
	}

}
