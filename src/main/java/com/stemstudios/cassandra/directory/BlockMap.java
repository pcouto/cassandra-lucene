package com.stemstudios.cassandra.directory;

import java.util.TreeMap;

import com.netflix.astyanax.serializers.StringSerializer;

/**
 * The <code>BlockMap</code> keeps track of file blocks by their names. Given
 * that the name is a byte array, we rely on a custom comparator that knows how
 * to compare such names.
 */
class BlockMap extends TreeMap<String, byte[]>
{
	private static final long serialVersionUID = 1550200273310875675L;

	/**
	 * Define a block map which is essentially a map of a block name (in the
	 * form of bytes) to the block data (again, in the form of bytes). Given
	 * that byte arrays don't lend themselves to comparison naturally, we pass
	 * it a custom comparator.
	 */
	public BlockMap()
	{
		super();
	}

	/**
	 * Get the value for the given key, which is a {@link java.lang.String}.
	 * 
	 * @param key
	 *            a stringified key
	 * @return the currently associated value
	 */
	public byte[] get(String key)
	{
		byte[] bKey = StringSerializer.get().toBytes(key);
		return super.get(bKey);
	}

	/**
	 * Put a <key, value> tuple in the block map, where the value is a
	 * {@link java.lang.String}.
	 * 
	 * @param key
	 *            a byte array key
	 * @param value
	 *            a stringified value
	 * @return the previously associated value
	 */
	public byte[] put(byte[] key, String value)
	{
		String sKey = StringSerializer.get().fromBytes(key);
		byte[] bValue = StringSerializer.get().toBytes(value);
		return super.put(sKey, bValue);
	}

	/**
	 * Put a <key, value> tuple in the block map, where the key is a
	 * {@link java.lang.String}.
	 * 
	 * @param key
	 *            a stringified key
	 * @param value
	 *            a byte array value
	 * @return the previously associated value
	 */
	public byte[] put(String key, byte[] value)
	{
		return super.put(key, value);
	}

	/**
	 * Put a <key, value> tuple in the block map, where both the key and value
	 * are a {@link java.lang.String}.
	 * 
	 * @param key
	 *            a stringified key
	 * @param value
	 *            a stringified value
	 * @return the previously associated value
	 */
	public byte[] put(String key, String value)
	{
		byte[] bValue = StringSerializer.get().toBytes(value);
		return super.put(key, bValue);
	}
}
