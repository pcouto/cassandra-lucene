package com.stemstudios.cassandra.directory;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.store.Lock;

import com.netflix.astyanax.recipes.locks.ColumnPrefixDistributedRowLock;

public class CassLock extends Lock
{
	private Log log = LogFactory.getLog(CassLock.class);
	private ColumnPrefixDistributedRowLock<String> lock;

	private boolean isLocked = false;

	public CassLock(CassandraDirectoryConfig config, String name)
	{
		lock = new ColumnPrefixDistributedRowLock<String>(config.getKeyspace(), config.getColumnFamily(), name).withConsistencyLevel(config.getLockConsistency()).expireLockAfter(config.getLockExpirationValue(), config.getLockExpirationUnit()).failOnStaleLock(false);
	}

	@Override
	public boolean isLocked() throws IOException
	{
		return isLocked;
	}

	@Override
	public boolean obtain() throws IOException
	{
		try
		{
			lock.acquire();
			log.info("Obtained Lock for " + lock.getKey());
		} catch (Exception e)
		{
			isLocked = false;
			log.error("Failed to obtain Lock for " + lock.getKey());
			return false;
		}
		isLocked = true;
		return true;
	}

	@Override
	public void release() throws IOException
	{
		try
		{
			lock.release();
			log.info("Released Lock for " + lock.getKey());
		} catch (Exception e)
		{
			throw new IOException(e);
		}
	}

}
