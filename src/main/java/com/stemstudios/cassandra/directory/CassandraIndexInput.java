package com.stemstudios.cassandra.directory;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.store.IndexInput;

public class CassandraIndexInput extends IndexInput {

	private Log log = LogFactory.getLog(CassandraIndexInput.class);
	private byte[] buffer;
	private int bufferPosition = 0;
	private int bufferLength = 0;

	private FileBlock currentLoadedBlock;

	private FileDescriptor fileDes;
	private ColumnOrientedFile columnOrientedFile;

	protected CassandraIndexInput(CassandraDirectory dir,
			FileDescriptor fileDesc) {
		super(fileDesc.getName());

		fileDes = fileDesc;
		columnOrientedFile = new ColumnOrientedFile(dir);
		log.info("Opened Index for file: " + fileDes.getName());
	}

	@Override
	public void close() throws IOException {
		log.info("Closed Index for file: " + fileDes.getName());
	}

	@Override
	public long getFilePointer() {
		
		return ((long) this.currentLoadedBlock.getBlockNumber()) * fileDes.getBlockSize()
				+ bufferPosition;
	}

	@Override
	public void seek(long pos) throws IOException {
		bufferPosition = (int) (pos % fileDes.getBlockSize());
		List<FileBlock> blocks = fileDes.getBlocks();
		for(FileBlock block : blocks)
		{
			long num = (long)block.getBlockNumber();
			long size = (long)block.getBlockSize();
			if(num*size > pos)
			{
				currentLoadedBlock = block;
				this.setBufferToCurrentBlock();
			}
		}
	}

	@Override
	public long length() {
		return fileDes.getLength();
	}

	@Override
	public byte readByte() throws IOException {
		if (bufferPosition >= bufferLength)
			nextBlock();
		return buffer[bufferPosition++];
	}

	private void nextBlock() throws IOException {
		if(this.currentLoadedBlock != null)
		{
			this.currentLoadedBlock = fileDes.getNextBlock(currentLoadedBlock);
		}
		else
		{
			this.currentLoadedBlock = fileDes.getFirstBlock();
		}
		setBufferToCurrentBlock();
	}

	private void setBufferToCurrentBlock() throws IOException {
		try {
			buffer = this.columnOrientedFile.readFileBlock(fileDes, currentLoadedBlock.getBlockName());
			if (buffer == null) {
				throw new IOException("Read past EOF: Block #"
						+ currentLoadedBlock.getBlockNumber() + " could not be found for file "
						+ fileDes.getName());
			}
		} catch (IOException ex) {
			throw new IOException("Read past EOF: Block #" + currentLoadedBlock.getBlockNumber()
					+ " could not be found for file " + fileDes.getName());
		}
		bufferLength = buffer.length;
	}

	@Override
	public void readBytes(byte[] b, int offset, int bytesToRead) throws IOException {
		if(buffer == null)
		{
			nextBlock();
		}
		while(bytesToRead >0)
		{
			int bytesToCopy = Math.min(bufferLength - bufferPosition, bytesToRead);
			System.arraycopy(buffer, bufferPosition, b, offset, bytesToCopy);
	         offset += bytesToCopy;
	         bytesToRead -= bytesToCopy;
	         bufferPosition += bytesToCopy;
	         if (bufferPosition >= bufferLength && bytesToRead > 0) {
	            nextBlock();
	            bufferPosition = 0;
	         }
		}
	}

}
