package com.stemstudios.cassandra.directory;

import java.io.IOException;

import org.apache.lucene.store.Lock;
import org.apache.lucene.store.LockFactory;

public class CassLockFactory extends LockFactory
{

	public void clearLock(CassandraDirectoryConfig config, String name) throws IOException
	{
		CassLock lock = new CassLock(config, name);
		lock.release();

	}

	@Override
	public void clearLock(String lockName) throws IOException
	{
		// TODO Auto-generated method stub

	}

	public Lock makeLock(CassandraDirectoryConfig config, String name)
	{
		CassLock lock = new CassLock(config, name);
		return lock;
	}

	@Override
	public Lock makeLock(String lockName)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
