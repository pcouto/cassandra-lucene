package com.stemstudios.cassandra.directory;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.store.IndexOutput;

/***
 * Class to implement Index output for Cassandra Directory. Code was based off
 * of infinispan implementation of Lucene directory. Will move code to be less
 * similar as concepts are understood
 * 
 * @author coutop
 * 
 */
public class CassandraIndexOutput extends IndexOutput {

	private Log log = LogFactory.getLog(CassandraIndexOutput.class);
	private byte[] buffer;
	private FileBlock currentBlock;

	private FileDescriptor fileDesc;

	private ColumnOrientedFile columnOrientedFile;

	public CassandraIndexOutput(CassandraDirectory dir, FileDescriptor fileDesc) {
		this.fileDesc = fileDesc;
		columnOrientedFile = new ColumnOrientedFile(dir);
		currentBlock = fileDesc.getFirstBlock();
		buffer = new byte[(int) currentBlock.getBlockSize()];
		log.info("Opened Index Output for "+fileDesc.getName());
	}

	@Override
	public void flush() throws IOException {
		this.storeCurrentBuffer();
	}

	@Override
	public void close() throws IOException {
		storeCurrentBuffer();
		log.info("Closed Index Output for "+fileDesc.getName());
	}

	private boolean isNewBlockNeeded() {
		return (currentBlock.getDataPosition() == currentBlock.getBlockSize());
	}

	@Override
	public long getFilePointer() {
		long num = currentBlock.getBlockNumber();
		long size = currentBlock.getBlockSize();
		return num * size + currentBlock.getDataPosition();
	}

	@Override
	public void seek(long pos) throws IOException {
		int newPos = (int) (pos % fileDesc.getBlockSize());
		List<FileBlock> blocks = fileDesc.getBlocks();
		for(FileBlock block : blocks)
		{
			long num = (long)block.getBlockNumber();
			long size = (long)block.getBlockSize();
			if(num*size > pos)
			{
				if(!block.equals(currentBlock))
				{
					this.storeCurrentBuffer();
				}
				currentBlock = block;				
			}
		}
		currentBlock.setDataPosition(newPos);
	}

	@Override
	public long length() throws IOException {
		return fileDesc.getLength();
	}

	@Override
	public void writeByte(byte b) throws IOException {
		if(this.isNewBlockNeeded())
		{
			this.newBlock();
		}
		buffer[currentBlock.getDataPosition()] = b;
		incrementPosition(1);
	}
	/***
	 * Helper Method to easily increment block position by i;
	 */
	private void incrementPosition(int i)
	{
		currentBlock.setDataPosition(currentBlock.getDataPosition()+i);
		if(currentBlock.getDataLength() < currentBlock.getDataPosition())
		{
			currentBlock.setDataLength(currentBlock.getDataPosition());
		}
		long num = currentBlock.getBlockNumber();
		long size = currentBlock.getBlockSize();
		long pos = currentBlock.getDataPosition();
		
		if(num*size+pos > fileDesc.getLength())
		{
			fileDesc.setLength(num*size+pos);
		}
	}
	
	private void newBlock() throws IOException {
		this.storeCurrentBuffer();
		currentBlock = fileDesc.createBlock();
		fileDesc.addLastBlock(currentBlock);
		buffer = new byte[(int) currentBlock.getBlockSize()];
	}

	@Override
	public void writeBytes(byte[] b, int offset, int length) throws IOException {
	      int writtenBytes = 0;
	      while (writtenBytes < length) {
	         if (isNewBlockNeeded()) {
	        	 newBlock();
	         }
	         int bSize = (int) currentBlock.getBlockSize();
	         int bPos = currentBlock.getDataPosition();
	         int pieceLength = Math.min(bSize - bPos, length - writtenBytes);
	         System.arraycopy(b, offset + writtenBytes, buffer, bPos, pieceLength);
	         incrementPosition(pieceLength);
	         writtenBytes += pieceLength;
	      }

	}

	/***
	 * Stores the Current buffer to persistent storage.
	 * 
	 * @param isClose
	 * @throws IOException
	 */
	protected void storeCurrentBuffer() throws IOException {
		byte[] bufferToFlush = buffer;

		// add chunk to cache
		if (currentBlock.getDataPosition() != 0) {
			// store the current chunk
			storeBufferAsChunk(bufferToFlush);
		}
	}

	/***
	 * Stores the provided byte array to Cassandra storage.
	 * 
	 * @param bufferToFlush
	 * @throws IOException
	 */
	private void storeBufferAsChunk(byte[] bufferToFlush)
			throws IOException {
		this.columnOrientedFile.writeFileBlock(fileDesc,
				currentBlock.getBlockName(), bufferToFlush);

	}
}
