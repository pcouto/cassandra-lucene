package com.stemstudios.cassandra.directory;

import java.util.concurrent.TimeUnit;

import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ConsistencyLevel;
import com.netflix.astyanax.serializers.StringSerializer;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;

/**
 * This class is used to build the configuration for the Cassandra Directory.
 * The class also houses the Astyanax context so the context is started and
 * shutdown within this class.
 * 
 * @author Phillip Couto
 * 
 */
public class CassandraDirectoryConfig
{
	private AstyanaxContext<Keyspace> _context;
	private Keyspace _keyspace;
	private ConsistencyLevel _DefaultConsistency = ConsistencyLevel.CL_QUORUM;
	private ConsistencyLevel _lockConsistency = ConsistencyLevel.CL_QUORUM;
	private ColumnFamily<String, String> _CF;
	private TimeUnit _lockExpireUnits = TimeUnit.MINUTES;
	private int _lockExpireTime = 60;
	private int _blockSize = 1;
	private int _blockSizeFactor = 1024 * 1024;

	/**
	 * Creates a config object with the provided Astyanax context and column
	 * family name
	 * 
	 * @param context
	 *            - Context used for connecting to Cassandra
	 * @param columnFamily
	 *            - Name of the column family to house the cassandra Directory
	 */
	public CassandraDirectoryConfig(AstyanaxContext<Keyspace> context, String columnFamily)
	{
		_context = context;
		_CF = ColumnFamily.newColumnFamily(columnFamily, new StringSerializer(), new StringSerializer());
	}

	/**
	 * Creates a config object using the provided varaibles and creates the
	 * context internally.
	 * 
	 * @param address
	 *            - of Cassandra node
	 * @param port
	 *            - for Cassandra node
	 * @param ClusterName
	 *            - for the cassandra cluster
	 * @param Keyspace
	 *            - name that will house the cassandra directory
	 * @param columnFamily
	 *            - Name of the column family that will house the cassandra
	 *            directory
	 */
	public CassandraDirectoryConfig(String address, int port, String ClusterName, String Keyspace, String columnFamily)
	{
		this(new AstyanaxContext.Builder().forCluster(ClusterName).forKeyspace(Keyspace).withAstyanaxConfiguration(new AstyanaxConfigurationImpl().setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE)).withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("MyConnectionPool").setPort(port).setMaxConnsPerHost(1).setSeeds(address + ":" + port)).withConnectionPoolMonitor(new CountingConnectionPoolMonitor()).buildKeyspace(ThriftFamilyFactory.getInstance()), columnFamily);
	}

	/**
	 * Gets the block size in bytes for use internally.
	 * 
	 * @return blocksize - calculated blocksize by multipling BlockSizeMB with
	 *         size of 1 MB
	 */
	protected int getBlockSize()
	{
		return this._blockSize * this._blockSizeFactor;
	}

	/**
	 * Gets the BlockSize in MegaBytes
	 * 
	 * @return int - BlockSize in MB
	 */
	public int getBlockSizeMB()
	{
		return this._blockSize;
	}

	/**
	 * Gets the column family object that is being used to store the cassandra
	 * directory.
	 * 
	 * @return ColumnFamily<String,String>
	 */
	public ColumnFamily<String, String> getColumnFamily()
	{
		return _CF;
	}

	/**
	 * Gets the default consistency level used by Cassandra directory for
	 * reading and writing data.
	 * 
	 * @return ConsistencyLevel
	 */
	public ConsistencyLevel getDefaultConsistency()
	{
		return _DefaultConsistency;
	}

	/**
	 * Gets the keyspace object used to interact with Cassandra
	 * 
	 * @return Keyspace
	 */
	public Keyspace getKeyspace()
	{
		return _keyspace;
	}

	/**
	 * Gets the lock consistency level used by cassandra directory when creating
	 * lock objects
	 * 
	 * @return ConsistencyLevel
	 */
	public ConsistencyLevel getLockConsistency()
	{
		return _lockConsistency;
	}

	/**
	 * Gets the Lock Expiration unit used when creating lock objects.
	 * 
	 * @return
	 */
	public TimeUnit getLockExpirationUnit()
	{
		return this._lockExpireUnits;
	}

	/**
	 * Gets the factor of time used to set the expiration time limit when
	 * locking objects are created.
	 * 
	 * @return
	 */
	public int getLockExpirationValue()
	{
		return this._lockExpireTime;
	}

	/**
	 * Sets the blocksize to be used when reading and writing blocks of files in
	 * cassandra directory. Changing the blocksize when reopening a cassandra
	 * directory will cause errors. If you would like to change the block size
	 * you will have to rebuild the index.
	 * 
	 * @param size
	 */
	public void setBlockSizeMB(int size)
	{
		this._blockSize = size;
	}

	/**
	 * Sets the default consistency level Cassandra directory should use when
	 * reading and writing data.
	 * 
	 * @param lvl
	 *            - The consistency level to use
	 */
	public void setDefaultConsistency(ConsistencyLevel lvl)
	{
		_DefaultConsistency = lvl;
	}

	/**
	 * Sets the consistency level Cassandra directory should use when creating
	 * lock objects.
	 * 
	 * @param lvl
	 */
	public void setLockConsistency(ConsistencyLevel lvl)
	{
		_lockConsistency = lvl;
	}

	/**
	 * Sets the Lock Expiration time and unit used when cassandra directory
	 * creates lock objects. This should be a reasonable value as it is used to
	 * prevent concurrent writes to files. Once the lock expires and lock
	 * acquisitions after that will clean up expired locks and acquire a new
	 * lock. The default is 60 minutes and should only be adjusted if you
	 * understand the consequences or need more time for bulk updates.
	 * 
	 * @param time
	 *            - quantity of the unit of time
	 * @param unit
	 *            - Unit of Time
	 */
	public void setLockExpirationUnit(int time, TimeUnit unit)
	{
		this._lockExpireUnits = unit;
	}

	/**
	 * Shuts down the context
	 */
	protected void Shutdown()
	{
		_keyspace = null;
		_context.shutdown();
	}

	/**
	 * Starts the context and retrieves the keyspace.
	 */
	protected void Start()
	{
		_context.start();
		_keyspace = _context.getEntity();
	}
}
