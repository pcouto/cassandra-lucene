package com.stemstudios.cassandra.directory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The <code>ColumnOrientedDirectory</code> captures the mapping of the concepts
 * of a directory to a column family in Cassandra. Specifically, it treats each
 * row in the column family as a file underneath the directory.
 * 
 * <p>
 * This class in turn relies on the {@link CassandraClient} for all low-level
 * gets and puts to the Cassandra server. More importantly, it does not require
 * that the {@link CassandraClient} to be familiar with the notion of Lucene
 * directories. Rather, it transparently translates those notions to column
 * families. In so doing, it ends up hiding the Cassandra layer from its
 * consumers.
 * </p>
 */
public class ColumnOrientedDirectory
{

	private CassandraClient cassandraClient;

	private List<String> systemColumns;

	private String descriptorColumn;

	private CassandraDirectoryConfig config;

	public ColumnOrientedDirectory(CassandraDirectory dir)
	{
		this.cassandraClient = dir.cassandraClient;
		this.descriptorColumn = CassandraDirectory.descriptorColumn;
		this.systemColumns = new ArrayList<String>();
		config = dir.config;
		systemColumns.add(descriptorColumn);
	}

	/**
	 * Return the file descriptor for the file of the given name. If the file
	 * cannot be found, then return null, instead of trying to create it.
	 * 
	 * @param fileName
	 *            the name of the file
	 * @return the descriptor for the given file
	 * @throws IOException
	 */
	protected FileDescriptor getFileDescriptor(String fileName) throws IOException
	{
		return getFileDescriptor(fileName, false);
	}

	/**
	 * Return the file descriptor for the file of the given name.
	 * 
	 * @param fileName
	 *            the name of the file
	 * @param createIfNotFound
	 *            if the file wasn't found, create it
	 * @return the descriptor for the given file
	 * @throws IOException
	 */
	protected FileDescriptor getFileDescriptor(String fileName, boolean createIfNotFound) throws IOException
	{
		FileDescriptor fileDescriptor = FileDescriptorUtils.fromBytes(config, cassandraClient.getColumn(fileName, descriptorColumn));

		if (fileDescriptor == null && createIfNotFound)
		{
			fileDescriptor = new FileDescriptor(fileName, config.getBlockSize());
			setFileDescriptor(fileDescriptor);
		}
		return fileDescriptor;
	}

	/**
	 * @return the names of the files in this directory
	 * @throws IOException
	 */
	public String[] getFileNames() throws IOException
	{
		List<String> keys = cassandraClient.getKeys(systemColumns);
		if (keys != null)
		{
			return keys.toArray(new String[] {});
		}
		return null;
	}

	/**
	 * Save the given file descriptor.
	 * 
	 * @param fileDescriptor
	 *            the file descriptor being saved
	 * @throws IOException
	 */
	public void setFileDescriptor(FileDescriptor fileDescriptor) throws IOException
	{
		BlockMap blockMap = new BlockMap();
		blockMap.put(descriptorColumn, FileDescriptorUtils.toString(fileDescriptor));
		cassandraClient.setColumns(fileDescriptor.getName(), blockMap);
	}
}
