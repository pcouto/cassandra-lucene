package com.stemstudios.cassandra.directory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.model.Column;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.model.Rows;

public class CassandraClient
{
	/**
	 * New Variables
	 */
	private CassandraDirectoryConfig _config;

	/***
	 * Uses the provided CassandraDirectoryConfig object to create a Cassandra
	 * Client
	 * 
	 * @param CassandraDirectoryConfig
	 */
	public CassandraClient(CassandraDirectoryConfig config)
	{
		this._config = config;
	}

	/**
	 * Gets the value of the column for the row provided.
	 * 
	 * @param fileName
	 *            RowKey
	 * @param columnName
	 *            Column Name
	 * @param CF
	 *            Column Family
	 * @return Value of the column for the provided row key.
	 * @throws IOException
	 */
	public byte[] getColumn(String fileName, String columnName) throws IOException
	{
		try
		{
			OperationResult<Column<String>> result = _config.getKeyspace().prepareQuery(_config.getColumnFamily()).setConsistencyLevel(_config.getDefaultConsistency()).getKey(fileName).getColumn(columnName).execute();
			Column<String> column = result.getResult();
			return column.getByteArrayValue();
		} catch (Exception ex)
		{
			// throw new IOException("Unable to read file descriptor for " +
			// fileName, ex);
			return null;
		}
	}

	/**
	 * Gets the specified column values for the row key provided.
	 * 
	 * @param key
	 *            Row Key
	 * @param columnNames
	 *            Name of columns to return
	 * @return Map of Column names and values
	 * @throws IOException
	 */
	public Map<String, byte[]> getColumns(String key, Set<String> columnNames) throws IOException
	{
		try
		{
			OperationResult<ColumnList<String>> result = _config.getKeyspace().prepareQuery(_config.getColumnFamily()).setConsistencyLevel(_config.getDefaultConsistency()).getKey(key).withColumnSlice(columnNames.toArray(new String[0])).execute();
			ColumnList<String> columns = result.getResult();
			Map<String, byte[]> colMap = new HashMap<String, byte[]>();
			for (Column<String> col : columns)
			{
				colMap.put(col.getName(), col.getByteArrayValue());
			}
			return colMap;
		} catch (Exception ex)
		{
			throw new IOException("Could not read from columns for file " + key, ex);
		}
	}

	/**
	 * New getKeys Method to use Astynax library
	 * 
	 * @param columnNames
	 * @return
	 * @throws IOException
	 */
	public List<String> getKeys(List<String> columnNames) throws IOException
	{
		try
		{
			OperationResult<Rows<String, String>> result = _config.getKeyspace().prepareQuery(_config.getColumnFamily()).setConsistencyLevel(_config.getDefaultConsistency()).getAllRows().withColumnSlice(columnNames.toArray(new String[0])).execute();
			List<String> keys = new ArrayList<String>();
			for (Row<String, String> row : result.getResult())
			{
				ColumnList<String> columns = row.getColumns();
				if (columns.size() > 0)
				{
					FileDescriptor fileDescriptor = FileDescriptorUtils.fromBytes(_config,columns.getColumnByIndex(0).getByteArrayValue());
					if (fileDescriptor == null || fileDescriptor.isDeleted())
					{
						continue;
					}
					keys.add(row.getKey());
				}
			}
			return keys;
		} catch (Exception e)
		{
			throw new IOException("Unable to list all files in " + _config.getKeyspace().getKeyspaceName());
		}
	}

	/**
	 * Sets the column data for the provided index file.
	 * 
	 * @param key
	 *            Row Key to update
	 * @param columnValues
	 *            Map of Column and values to update
	 * @throws IOException
	 */
	protected void setColumns(String key, Map<String, byte[]> columnValues) throws IOException
	{
		MutationBatch m = _config.getKeyspace().prepareMutationBatch();
		m.setConsistencyLevel(_config.getDefaultConsistency());

		if (columnValues == null || columnValues.size() == 0)
		{
			m.withRow(_config.getColumnFamily(), key).delete();
		} else
		{
			for (Entry<String, byte[]> columnValue : columnValues.entrySet())
			{
				String column = columnValue.getKey();
				byte[] value = columnValue.getValue();

				if (value == null)
				{
					if (column != null)
					{
						m.withRow(_config.getColumnFamily(), key).deleteColumn(column);
					}
				} else
				{
					m.withRow(_config.getColumnFamily(), key).putColumn(column, value, null);
				}
			}
		}
		try
		{
			m.execute();
		} catch (Exception ex)
		{
			throw new IOException("Unable to mutate columns for file " + key, ex);
		}

	}
}
